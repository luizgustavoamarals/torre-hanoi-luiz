import java.util.Stack;

public class Torre {
    public static void main(String[] args) {

       //Criando as torres do jogo
        Stack <Integer> original = new Stack<>();
        Stack <Integer> destino = new Stack<>();
        Stack <Integer> aux = new Stack<>();

       //Adcionando numero de pe�as
        original.push(3);
        original.push(2);
        original.push(1);

        torreDeHanoi(original.size(), original, destino, aux);

    }

    public static void torreDeHanoi(int p, Stack<Integer> original, Stack<Integer> destino, Stack<Integer> aux) {

        if(p > 0){
            torreDeHanoi(p-1, original, aux, destino);
            destino.push(original.pop());
            System.out.println("---------");
            System.out.println("Torre A: " + original);
            System.out.println("Torre B: "+aux);
            System.out.println("Torre C: " + destino);

            torreDeHanoi(p-1,aux,destino,original);

        }
    }
}